package com.example.kostja.weatherapp;

import android.graphics.drawable.Drawable;
import android.media.Image;

import java.util.Date;

public class XmlForecast {

    private String _date = null;
    private String _maxTemp = null;
    private String _minTemp = null;
    private String _icon = null;


    XmlForecast()
    {
    }

    void setDate(String date) {
        _date = date;
    }
    void setMaxTemp(String maxTemp) {
        _maxTemp = maxTemp;
    }
    void setMinTemp(String minTemp) {
        _minTemp = minTemp;
    }
    void setIcon(String icon) {
        _icon = icon;
    }

    String getDate() {
        return _date;
    }
    String getMaxTemp() {
        return _maxTemp;
    }
    String getMinTemp() {
        return _minTemp;
    }
    String getIcon() {
        return _icon;
    }


    /*
    public String toString()
    {
        // limit how much text we display
        if (_title.length() > 42)
        {
            return _title.substring(0, 42) + "...";
        }
        return _title;
    }
    */

}
