package com.example.kostja.weatherapp;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class WeatherXmlParser {
    // We don't use namespaces
    private static final String ns = null;


    public List parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, ns, "root");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String strName = parser.getName();
            // Starts by looking for the forecastday tag
            if (strName.equals("forecastday")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    private String readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {

        String entries = null;
        parser.require(XmlPullParser.START_TAG, ns, "forecastday");
        String name = null;
        String date = null;
        String maxtemp_c = null;
        String mintemp_c = null;
        String icon = null;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String strName = parser.getName();
            switch (strName) {
                case "name":
                    entries = readName(parser);
                    //name = readName(parser);
                    break;
                case "date":
                    entries = readDate(parser);
                    //date = readDate(parser);
                    break;
                case "maxtemp_c":
                    entries = readTemp(parser);
                    //maxtemp_c = readTemp(parser);
                    break;
                case "mintemp_c":
                    entries = readTemp(parser);
                    //mintemp_c = readTemp(parser);
                    break;
                case "icon":
                    entries = readIcon(parser);
                    //icon = readIcon(parser);
                    break;
                default:
                    skip(parser);
            }
        }
        return entries;
    }



    // Processes title tags in the feed.
    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return name;
    }

    // Processes title tags in the feed.
    private String readDate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "date");
        String date = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "date");
        return date;
    }

    // Processes link tags in the feed.
    private String readTemp(XmlPullParser parser) throws IOException, XmlPullParserException {

        String name = null;

        if (parser.getName().equals("mintemp_c")) {
            name = "mintemp_c";
        } else {
            name = "maxtemp_c";
        }

        parser.require(XmlPullParser.START_TAG, ns, parser.getName().toString());
        String temp = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, parser.getName().toString());

        return temp;
    }

    // Processes summary tags in the feed.
    private String readIcon(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "icon");
        String summary = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "icon");
        return summary;
    }

    // For the tags title and summary, extracts their text values.
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
