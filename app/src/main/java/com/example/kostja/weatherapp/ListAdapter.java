package com.example.kostja.weatherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ListAdapter extends BaseAdapter {
    private final List<XmlForecast> items;

    public ListAdapter(final List<XmlForecast> items) {
        this.items = items;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return this.items.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return this.items.get(arg0);
    }

    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        View itemView = null;
        final XmlForecast row = this.items.get(arg0);
        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.feed_item, null);
        } else {
            itemView = arg1;
        }
        TextView forecast_icon = (TextView) itemView.findViewById(R.id.forecast_icon);
        forecast_icon.setText(row.getIcon());

        TextView forecast_max = (TextView) itemView.findViewById(R.id.forecast_max);
        forecast_max.setText(row.getMaxTemp());

        TextView forecast_min = (TextView) itemView.findViewById(R.id.forecast_min);
        forecast_min.setText(row.getMinTemp());

        TextView forecast_date = (TextView) itemView.findViewById(R.id.forecast_date);
        forecast_date.setText(row.getDate());

        return itemView;
    }

}