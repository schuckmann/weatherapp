package com.example.kostja.weatherapp;

import android.graphics.drawable.Drawable;
import android.media.Image;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class XmlHandler extends DefaultHandler {
        XmlFeed feed;
        XmlForecast item;

        int depth = 0;
        //outer
        final int ROOT = 1;
        final int FORECAST_LOCATION = 2;
        final int FORECASTDAY = 3;
        final int DAY = 4;
        final int CONDITION = 5;

        int curr_element = 0;
        //options
        final int NAME = 1;
        final int COUNTRY = 2;
        final int DATE = 3;
        final int MIN_TEMP = 4;
        final int MAX_TEMP = 5;
        final int ICON = 6;

        XmlHandler()
        {
        }
        XmlFeed getFeed()
        {
            return feed;
        }

        public void startDocument() throws SAXException
        {
            feed = new XmlFeed();
            depth = 0;

        }
        public void endDocument() throws SAXException
        {
        }
        public void startElement(String namespaceURI, String localName,String qName, Attributes atts) throws SAXException
        {
            if (localName.equals("root"))
            {
                depth++; //ROOT
                return;
            }
            if(localName.equals("location")){
                depth++;//LOCATION
                item = new XmlForecast();
                return;
            }
            if (localName.equals("forecast")) // forecast same depht as location
            {
                depth++;//FORECAST
                return;
            }
            if (localName.equals("forecastday"))
            {
                depth++;//ITEM
                item = new XmlForecast();
                return;
            }
            if (localName.equals("day"))
            {
                depth++;//ITEM
                item = new XmlForecast();
                return;
            }
            if (localName.equals("condition"))
            {
                depth++;//ITEM
                item = new XmlForecast();
                return;
            }

            switch(depth){
                case FORECAST_LOCATION:
                    if (localName.equals("name"))
                    {
                        curr_element = NAME;
                    }else if (localName.equals("country"))
                    {
                        curr_element = COUNTRY;
                    }
                    break;
                case FORECASTDAY:
                    if (localName.equals("date"))
                    {
                        curr_element = DATE;
                    }
                    break;
                case DAY:
                    if (localName.equals("maxtemp_c"))
                    {
                        curr_element = MAX_TEMP;
                    }else if (localName.equals("mintemp_c"))
                    {
                        curr_element = MIN_TEMP;
                    }
                    break;
                case CONDITION:
                    if (localName.equals("icon"))
                    {
                        curr_element = ICON;
                    }
                    break;
            }
        }

        public void endElement(String namespaceURI, String localName, String qName) throws SAXException
        {
            if (localName.equals("condition"))
            {	feed.addItem(item);
                depth--;
            }else if (localName.equals("day"))
            {
                //feed.addItem(item);
                depth--;
            } else if (localName.equals("forecastday"))
            {
                //feed.addItem(item);
                depth--;
            } else if (localName.equals("location"))
            {
                //feed.addItem(item);
                depth--;
            } else if (localName.equals("forecast"))
            {
                depth--;
            } else if (localName.equals("root"))
            {
                depth--;
            }
        }

        public void characters(char ch[], int start, int length)
        {
            String theString = new String(ch,start,length);

            switch(depth){
                case FORECAST_LOCATION:
                    switch(curr_element) {
                        case NAME:
                            feed.setName(theString);
                            break;
                        case COUNTRY:
                            feed.setCountry(theString);
                            break;
                    }
                    curr_element = 0;
                case FORECASTDAY:
                    switch(curr_element) {
                        case DATE:
                            item.setDate(theString);
                            break;
                    }
                    curr_element = 0;
                    break;
                case DAY:
                    switch(curr_element) {
                        case MAX_TEMP:
                            item.setMaxTemp(theString);
                            break;
                        case MIN_TEMP:
                            item.setMinTemp(theString);
                            break;
                    }
                    curr_element = 0;
                    break;
                case CONDITION:
                    switch (curr_element){
                        case ICON:
                            item.setIcon(theString);
                            break;
                    }
                    curr_element = 0;
                    break;
            }
        }

}
