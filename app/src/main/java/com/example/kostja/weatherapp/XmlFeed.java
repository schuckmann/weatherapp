package com.example.kostja.weatherapp;

import java.util.ArrayList;
import java.util.List;

public class XmlFeed {
    private String _name = null;
    private String _country = null;

    private List<XmlForecast> _itemlist;


    XmlFeed()
    {
        _itemlist = new ArrayList<XmlForecast>();
    }
    void addItem(XmlForecast item)
    {
        _itemlist.add(item);
    }
    XmlForecast getItem(int position)
    {
        return _itemlist.get(position);
    }
    List<XmlForecast> getAllItems()
    {
        return _itemlist;
    }
    int getItemCount()
    {
        return _itemlist.size();
    }

    void setName(String name)
    {
        _name = name;
    }
    void setCountry(String country) {
        _country = country;
    }

    String getName()
    {
        return _name;
    }

    String getCountry(){
        return  _country;
    }


}
