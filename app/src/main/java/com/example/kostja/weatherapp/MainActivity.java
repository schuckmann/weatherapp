package com.example.kostja.weatherapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.BreakIterator;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MainActivity extends AppCompatActivity {

    private EditText editCity;
    private TextView displayCity;
    private ListView listView;
    private XmlFeed feed = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editCity = (EditText) findViewById(R.id.editCity);
        Button btGet = (Button) findViewById(R.id.buttonGet);
        listView = (ListView) findViewById(R.id.listView);
        displayCity = (TextView) findViewById(R.id.displayCity);

        editCity.setText("Berlin"); // initial set

        btGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayCity.setText(editCity.getText().toString()); // set this properly

                URL url = null;
                try {
                    url = new URL("https://api.apixu.com/v1/forecast.xml?key=f6c739186365406f81f215626170506&q=" +
                            editCity.getText().toString() + ",pt&days=7");
                    getXmlfeed(url);
                    //getParsedData(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                //

            }
        });

    }

    private void UpdateDisplay() {

        if (feed == null){
            displayCity.setText("No Xml available!");
            return;
        }
        displayCity.setText(feed.getName() + ", " + feed.getCountry());
        //ListAdapter adapter = new ListAdapter(feed.getAllItems());

        com.example.kostja.weatherapp.ListAdapter adapter = new com.example.kostja.weatherapp.ListAdapter(feed.getAllItems());
        listView.setAdapter(adapter);
        /*
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DetailsActivity.class);
                i.putExtra("TITLE", feed.getItem(position).getTitle());
                i.putExtra("PUBDATE", feed.getItem(position).getPubDate());
                i.putExtra("DESCRIPTION", feed.getItem(position).getDescription());
                i.putExtra("LINK", feed.getItem(position).getLink());
                startActivity(i);
            }
        });
        */
        listView.setSelection(0);
    }


    private void getXmlfeed(final URL url) {
        AsyncTask<URL, Void, XmlFeed> task = new AsyncTask<URL, Void, XmlFeed>() {

            @Override
            protected XmlFeed doInBackground(URL... urls) {
                try {
                    // create the factory
                    SAXParserFactory factory = SAXParserFactory.newInstance();
                    // create a parser
                    SAXParser parser = factory.newSAXParser();
                    // create the reader (scanner)
                    XMLReader xmlreader = parser.getXMLReader();
                    // instantiate our handler
                    XmlHandler handler = new XmlHandler();
                    // assign our handler
                    xmlreader.setContentHandler(handler);
                    // get our data via the url class
                    InputSource is = new InputSource(url.openStream());
                    // perform the synchronous parse
                    xmlreader.parse(is);
                    // get the results - should be a fully populated RSSFeed instance, or null on error
                    return handler.getFeed();
                } catch (Exception ee) {
                    Toast.makeText(getApplicationContext(), "Error: " + ee.toString(), Toast.LENGTH_SHORT).show();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(XmlFeed rssFeed) {
                super.onPostExecute(rssFeed);
                feed = rssFeed;
                UpdateDisplay();
            }
        };


        task.execute((URL[]) null);
    }
}

            /*

            private void getParsedData(URL url) {
                AsyncTask<URL,Void,String> task = new AsyncTask<URL, Void, String>() {
                    @Override
                    protected String doInBackground(URL... urls) {
                        try {
                            // create the factory
                            SAXParserFactory factory = SAXParserFactory.newInstance();
                            // create a parser
                            SAXParser parser = factory.newSAXParser();
                            // create the reader (scanner)
                            XMLReader xmlreader = parser.getXMLReader();
                            // instantiate our handler
                            //XMLReader handler = new XMLReader();
                            // assign our handler
                            //xmlreader.setContentHandler(handler);
                            // get our data via the url class
                            InputSource is = new InputSource(urls[0].openStream());
                            // perform the synchronous parse
                            xmlreader.parse(is);
                            // get the results - should be a fully populated RSSFeed instance, or null on error
                            return xmlreader.toString();
                        } catch (Exception ee) {
                            Toast.makeText(getApplicationContext(),"Error: "+ee.toString(), Toast.LENGTH_SHORT).show();
                            return null;
                        }

                    }

                    @Override
                    protected void onPostExecute(WeatherXmlParser str) {
                        super.onPostExecute(String.valueOf(str));
                        weatherXmlParser = str;
                        UpdateDisplay();
                    }
                };
                task.execute( (URL[]) null);
            }
        */
    /*
    }
    private void UpdateDisplay() {

        if (weatherXmlParser == null){
            editCity.setText("Nothing to Parse");
            return;
        }


        ListAdapter  adapter = new ListAdapter(weatherXmlParser.g   .getAllItems());
        editCity.setAdapter(adapter);
        /*
        editCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getApplicationContext(), DetailsActivity.class);
                i.putExtra("TITLE", feed.getItem(position).getTitle());
                i.putExtra("PUBDATE", feed.getItem(position).getPubDate());
                i.putExtra("DESCRIPTION", feed.getItem(position).getDescription());
                i.putExtra("LINK", feed.getItem(position).getLink());
                startActivity(i);
            }
        });

        editCity.setSelection(0);

    }

}
*/

